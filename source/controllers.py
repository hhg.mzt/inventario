from flask_restful import Resource, request
from .services import ProductService
from .validators import ProductValidator


class ProductListController(Resource, ProductValidator):
    def __init__(self):
        self.product_service = ProductService()

    def get(self):
        products = self.product_service.fetch()
        return ({'data': products}, 200) if products else ({'data': []}, 404)

    def post(self):
        payload = self.parser.parse_args()
        payload['fecha_modifico'] = None
        payload['usuario_modifico'] = None
        create_product = self.product_service.create(payload)
        if not create_product[0]:
            return {'data': None, 'message': 'Ocurrio un error al guardar el producto'}, 500
        return {'data': create_product[1], 'message': 'success'}, 201


class ProductController(Resource, ProductValidator):

    def __init__(self):
        self.produc_service = ProductService()

    def get(self, id_product):
        product = self.produc_service.retrieve(id_product)
        return ({'data': product}, 200) if product else \
            ({'data': None, 'message': 'No se encontró el producto'}, 404)

    def put(self, id_product):
        payload = self.parser.parse_args()
        if not payload['usuario_modifico']:
            return {'message': 'Capture el usuario que modifica'}, 400
        if not payload['fecha_modifico']:
            return {'message': 'Capture la fecha'}, 400
        del payload['usuario_registro']
        payload = self.produc_service.modify(id_product, **payload)
        return ({'data': payload}, 200) if payload else \
            ({'data': None, 'message': 'Ocurrio un error al modificar'}, 400)

    def delete(self, id_product):
        product = self.produc_service.retrieve(id_product)
        self.produc_service.delete(id_product)
        return {'data': product, 'message': 'Producto Eliminado'}, 200

