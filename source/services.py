from .models import Product


class ProductService:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def create(self, kwargs):
        new_product = Product(**kwargs)
        return (True, new_product.to_dict()) if new_product.save() else (False, None)

    def fetch(self):
        products = Product.query.all()
        return [product.to_dict() for product in products]

    def retrieve(self, pk_product):
        product = Product.query.get(pk_product)
        return None if not product else product.to_dict()

    def modify(self, pk, **kwargs):
        modify = Product.update(pk, kwargs)
        return None if not modify[0] else modify[1]

    def delete(self, pk):
        item_before_delete = Product.query.get(pk)
        if not item_before_delete:
            return None
        Product.delete(Product, item_before_delete)
        return item_before_delete.to_dict()
