import os
from flask import Flask, Blueprint
from flask_restful import Api
from .extensions import db
from .controllers import ProductListController, ProductController

app = Flask(__name__)

app.config.from_json(os.path.abspath(os.path.join('settings.json')))
db.init_app(app)

resources = Blueprint('resources', __name__)
api = Api(resources, prefix='/api/')
api.add_resource(ProductListController, 'products')
api.add_resource(ProductController, 'product/<int:id_product>')

app.register_blueprint(resources)
