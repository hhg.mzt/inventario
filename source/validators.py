from flask_restful import reqparse


class ProductValidator:
    parser = reqparse.RequestParser()
    parser.add_argument('codigo_barra', type=str)
    parser.add_argument('nombre', type=str, required=True, help='Favor de poner un producto.')
    parser.add_argument('id_grupo_proveedor', type=int,
                        required=True, help='Favor de seleccionar un proveedor.')
    parser.add_argument('id_linea', type=int, required=True, help='Favor de seleccionar una linea.')
    parser.add_argument('id_familia', type=int, required=True, help='Favor de seleccionar una familia.')
    parser.add_argument('id_subfamilia', type=int, required=True,
                        help='Favor de seleccionar una subfamilia.')
    parser.add_argument('id_clasificacion', type=int, required=True,
                        help='Favor de seleccionar una clasificación.')

    parser.add_argument('iva', type=int)
    parser.add_argument('ieps', type=int)
    parser.add_argument('usuario_registro', type=int)
    parser.add_argument('activo', type=int, required=True, help='Campo activo es requerido.')
    parser.add_argument('usuario_modifico', type=str)
    parser.add_argument('fecha_modifico', type=str)

