from sqlalchemy import (
    exc,
    inspect,
    Column,
    Integer,
    VARCHAR,
    Date,
    DateTime,
    func
)
from .extensions import db


class CRUD(object):
    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            return False

    @classmethod
    def update(cls, ident, dict_changes):
        try:
            obj = cls.query.get_or_404(ident)
            # asigmanos los atributos
            for key, value in dict_changes.items():
                setattr(obj, key, value)

            db.session.commit()
            return True, obj.to_dict()
        except exc.SQLAlchemyError as e:
            print(e)
            db.session.rollback()
            return False, None

    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()

    def __get_value(self, key):
        if 'fecha' in key or 'hora' in key:
            return str(getattr(self, key))
        else:
            import decimal
            if isinstance(getattr(self, key), decimal.Decimal):
                return float(getattr(self, key))
            elif isinstance(getattr(self, key), bool):
                return int(getattr(self, key))
            else:
                return getattr(self, key)

    def to_dict(self):
        return {c.key: self.__get_value(c.key)
                for c in inspect(self).mapper.column_attrs}


class Product(db.Model, CRUD):
    __tablename__ = 'productos_venta'
    __table_args__ = {'implicit_returning': False}
    id_producto = Column(Integer, primary_key=True)
    codigo_barra = Column(VARCHAR(length=20), nullable=True)
    nombre = Column(VARCHAR(length=50), nullable=False)
    id_grupo_proveedor = Column(Integer)
    id_linea = Column(Integer)
    id_familia = Column(Integer, nullable=False)
    id_subfamilia = Column(Integer,  nullable=False)
    id_clasificacion = Column(Integer)
    iva = Column(Integer, nullable=True)
    ieps = Column(Integer, nullable=True)
    fecha_registro = Column(DateTime(), default=func.now())
    usuario_registro = Column(Integer, nullable=False)
    activo = Column(Integer, nullable=False)
    usuario_modifico = Column(Integer, nullable=True)
    fecha_modifico = Column(Date(), nullable=True)

    def __init__(self, codigo_barra, nombre, id_grupo_proveedor, id_linea,
                 id_familia, id_subfamilia, id_clasificacion,
                 iva, ieps, usuario_registro, activo, usuario_modifico,
                 fecha_modifico):
        self.codigo_barra = codigo_barra
        self.nombre = nombre
        self.id_grupo_proveedor = id_grupo_proveedor
        self.id_linea = id_linea
        self.id_familia = id_familia
        self.id_subfamilia = id_subfamilia
        self.id_clasificacion = id_clasificacion
        self.iva = iva
        self.ieps = ieps
        self.usuario_registro = usuario_registro
        self.activo = activo
        self.usuario_modifico = usuario_modifico
        self.fecha_modifico = fecha_modifico

    def __repr__(self):
        return "%r" % self.__dict__
