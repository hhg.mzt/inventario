USE [dsd_mazatlanWeb]
GO
/****** Object:  Table [dbo].[productos_venta]    Script Date: 23/03/2021 05:36:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productos_venta](
	[id_producto] [int] IDENTITY(1,1) NOT NULL,
	[codigo_barra] [varchar](20) NULL,
	[nombre] [varchar](50) NOT NULL,
	[id_grupo_proveedor] [int] NOT NULL,
	[id_linea] [int] NOT NULL,
	[id_familia] [int] NOT NULL,
	[id_subfamilia] [int] NOT NULL,
	[id_clasificacion] [int] NOT NULL,
	[iva] [bit] NULL,
	[ieps] [bit] NULL,
	[fecha_registro] [date] NOT NULL,
	[usuario_registro] [int] NOT NULL,
	[activo] [bit] NULL,
	[usuario_modifico] [int] NULL,
	[fecha_modifico] [date] NULL
) ON [PRIMARY]
GO
